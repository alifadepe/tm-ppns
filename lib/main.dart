import 'package:flutter/material.dart';
import 'package:tm_ppns/page/example.dart';
import 'package:tm_ppns/page/list_job.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Demo',
      home: ListJobPage(),
    );
  }
}
