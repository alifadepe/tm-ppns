import 'package:flutter/material.dart';

class UnorderedList extends StatelessWidget {
  final Text text;

  const UnorderedList({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: 10,
          height: 10,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
          ),
        ),
        Expanded(
          child: text,
        ),
      ],
    );
  }
}