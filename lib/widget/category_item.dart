import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tm_ppns/widget/category_container.dart';

class CategoryItem extends StatelessWidget {
  final String title;
  final int jobCount;
  final Widget image;

  const CategoryItem({
    Key? key,
    required this.title,
    required this.jobCount,
    this.image = const Placeholder(
      color: Color.fromARGB(24, 0, 0, 0),
    ),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CategoryContainer(
      padding: const EdgeInsets.all(10),
      child: Column(
        children: [
          Container(
            width: 40,
            height: 40,
            margin: const EdgeInsets.only(
              top: 8,
              bottom: 8,
            ),
            child: image,
          ),
          Text(
            title,
            style: GoogleFonts.roboto(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              color: HexColor("#000000"),
            ),
          ),
          const SizedBox(
            height: 4,
          ),
          Text(
            "($jobCount Tantangan Karir)",
            style: GoogleFonts.roboto(
              fontSize: 14,
              fontWeight: FontWeight.normal,
              color: HexColor("#979797"),
            ),
          ),
        ],
      ),
    );
  }
}
