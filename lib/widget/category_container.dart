import 'package:flutter/material.dart';

class CategoryContainer extends StatelessWidget {
  final Widget child;
  final EdgeInsets? margin;
  final EdgeInsets? padding;

  const CategoryContainer({
    required this.child,
    this.margin,
    this.padding,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: const Color.fromARGB(24, 0, 0, 0),
        ),
      ),
      padding: padding,
      margin: margin,
      child: child,
    );
  }
}
