import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:money_formatter/money_formatter.dart';
import 'package:tm_ppns/widget/category_container.dart';

class JobContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CategoryContainer(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                margin: const EdgeInsets.only(
                  right: 8,
                ),
                child: Image.asset(
                  "assets/image/tokopedia.png",
                  width: 52,
                  height: 52,
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "UI UX Designer",
                      style: GoogleFonts.roboto(
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        color: HexColor("#000000"),
                      ),
                    ),
                    Text(
                      "Tokopedia",
                      style: GoogleFonts.roboto(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: HexColor("#7A7A7A"),
                      ),
                    ),
                  ],
                ),
              ),
              const Icon(
                Icons.bookmark_border,
                size: 24,
              ),
            ],
          ),
          Container(
            margin: const EdgeInsets.only(
              top: 21,
              bottom: 14,
            ),
            child: Text(
              "Jakarta Utara",
              style: GoogleFonts.roboto(
                fontSize: 20,
                fontWeight: FontWeight.w700,
                color: HexColor("#000000"),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(
                  right: 8,
                ),
                child: const Icon(Icons.money),
              ),
              Text(
                _formatSalary(1000000) + " - " + _formatSalary(1000000),
                style: GoogleFonts.roboto(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: HexColor("#151515"),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(
                  right: 8,
                ),
                child: const Icon(Icons.access_time),
              ),
              Expanded(
                child: Text(
                  "2-3 tahun pengalaman",
                  style: GoogleFonts.roboto(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: HexColor("#151515"),
                  ),
                ),
              ),
              Text(
                "Full time",
                style: GoogleFonts.roboto(
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                  color: HexColor("#EA9303"),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  String _formatSalary(double salary) {
    return MoneyFormatter(
      amount: salary,
    ).output.withoutFractionDigits.toString();
  }
}
