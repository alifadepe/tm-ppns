import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class Example extends StatelessWidget {
  const Example({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _topSection(context),
              // _categorySection(context),
              // _jobSection(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _topSection(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
            "Temukan Pekerjaan Impianmu!",
            style: GoogleFonts.roboto(
              color: HexColor("#CFCFCF"),
              fontWeight: FontWeight.w400,
              fontSize: 24,
            ),
          ),
          TextField(
            decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.search,
                color: HexColor("#CACACA"),
              ),
              hintText: "Cari Tantangan Karir",
              hintStyle: GoogleFonts.roboto(
                color: HexColor("#CACACA"),
                fontWeight: FontWeight.w400,
                fontSize: 16,
              ),
              fillColor: HexColor("#35485B"),
              filled: true,
            ),
          ),
        ],
      ),
      width: double.maxFinite,
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
        top: 38,
        bottom: 38,
      ),
      decoration: BoxDecoration(
        color: HexColor("#032549"),
      ),
    );
  }
}
