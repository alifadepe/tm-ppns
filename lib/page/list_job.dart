import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tm_ppns/widget/category_container.dart';
import 'package:tm_ppns/widget/category_item.dart';
import 'package:tm_ppns/widget/job_container.dart';

class ListJobPage extends StatelessWidget {
  const ListJobPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _topSection(context),
              _categorySection(context),
              _jobSection(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _topSection(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        left: 20,
        right: 20,
        top: 38,
        bottom: 31,
      ),
      decoration: BoxDecoration(
        color: HexColor("#032549"),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Temukan Pekerjaan Impianmu!",
            style: GoogleFonts.roboto(
              fontSize: 24,
              fontWeight: FontWeight.w400,
              color: HexColor("#CFCFCF"),
            ),
          ),
          const SizedBox(
            height: 26,
          ),
          TextField(
            decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.search,
                color: HexColor("#CACACA"),
              ),
              hintText: "Cari Tantangan Karir",
              hintStyle: GoogleFonts.roboto(
                color: HexColor("#CACACA"),
                fontWeight: FontWeight.w400,
                fontSize: 16,
              ),
              fillColor: HexColor("#35485B"),
              filled: true,
            ),
          ),
        ],
      ),
    );
  }

  Widget _categorySection(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 30,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Kategori",
            style: GoogleFonts.roboto(
              fontSize: 20,
              fontWeight: FontWeight.w700,
              color: HexColor("#000000"),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            children: const [
              Expanded(
                child: CategoryItem(
                  title: "Marine",
                  jobCount: 52,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: CategoryItem(
                  title: "Mining",
                  jobCount: 48,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: const [
              Expanded(
                child: CategoryItem(
                  title: "Konstruksi",
                  jobCount: 30,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: CategoryItem(
                  title: "Manufaktur",
                  jobCount: 45,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _jobSection(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 30,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Tantangan Karir Terbaru",
            style: GoogleFonts.roboto(
              fontSize: 20,
              fontWeight: FontWeight.w700,
              color: HexColor("#000000"),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          JobContainer(),
        ],
      ),
    );
  }
}
